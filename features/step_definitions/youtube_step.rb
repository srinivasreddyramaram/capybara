Given("I am on youtube home page") do
	visit 'https://www.youtube.com'
end

When("I search for {string}") do |string|
  fill_in 'search_query', :with => 'capybara'
  click_on 'Search'
end

Then("videos of large rodents are returned") do
	expect(page).to have_content('Capybara')
end
